name := "scala-macros-playground"

version := "0.1"

organization := "me.adinapoli"

scalaVersion := "2.10.0"

/** Shell */
shellPrompt := { state => System.getProperty("user.name") + "> " }

shellPrompt in ThisBuild := { state => Project.extract(state).currentRef.project + "> " }

/** Dependencies */
resolvers += "snapshots-repo" at "http://scala-tools.org/repo-snapshots"

resolvers += "spray repo" at "http://repo.spray.io"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "Sonatype OSS Releases" at "http://oss.sonatype.org/content/repositories/releases/"

resolvers += "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies <<= scalaVersion { scala_version => 
  val scalazVersion = "7.0.0-M7"
  Seq(
    "org.scala-lang"       % "scala-reflect"                   % "2.10.0",
    "org.scala-lang"       % "scala-compiler"                   % "2.10.0",
    "com.typesafe"         % "slick_2.10"                      % "1.0.0-RC1",
    "org.scalaz"           % "scalaz-core_2.10"                % scalazVersion
  )
}

/** Compilation */
javaOptions += "-Xmx2G"

scalacOptions ++= Seq("-deprecation", "-unchecked", "-feature", "-language:experimental.macros")

maxErrors := 20 

pollInterval := 1000

logBuffered := false

cancelable := true
