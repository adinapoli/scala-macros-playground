package me.adinapoli

/* Simplest macro ever. */

import reflect.macros.Context
import language.experimental.macros

object Id {
  def id[T <: AnyRef](a: T): a.type = macro IdImpl.idImpl[T]
}

object IdImpl {
  def idImpl[T](c: Context)(a: c.Expr[T]): c.Expr[T] = a
}
