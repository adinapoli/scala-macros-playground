package me.adinapoli

/** A simple version of assert **/

import reflect.macros.Context
import language.experimental.macros

object AssertTrue {
  def assertTrue(cond: Boolean): Unit = macro AssertTrueImpl.assertTrueImpl
}

//Took here: https://github.com/retronym/macrocosm
object AssertTrueImpl {
  def assertTrueImpl(c: Context)(cond: c.Expr[Boolean]): c.Expr[Unit] = {
    import c.universe._
    val condCode = c.Expr[String](Literal(Constant(show(cond.tree))))
    c.universe.reify {
      assert(cond.splice, condCode.splice)
      ()
    }
  }


}
